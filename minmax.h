/*
* Donnée : Cette fonction est conçue pour accepter un tableau d'entiers T et sa taille n en entrée
* Résultat : la valeur minimale et maximale du tableau d'entrée sont stockées dans les adresses pointés par pmin et pmax, respectivement. Dans le cas d'un tableau vide (n = 0), les valeurs de pmin et pmax sont INT_MAX et INT_MIN, respectivement.
 */

void minmax(int *t, int n, int *pmin, int *pmax);

