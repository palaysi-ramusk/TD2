#include <stdio.h>
#include <malloc.h>
/**
* Donnée : Cette fonction est conçue pour accepter un tableau d'entiers et sa longueur en entrée, puis pour créer un nouveau tableau qui ne contient que les nombres négatifs du tableau d'entrée.
*/

/**
* Résultat : La fonction renvoie un nouveau tableau dynamique qui stocke tous les nombres négatifs du tableau d'entrée.
*/

int *nvtab(int T[], int n){
    int *tab = malloc((sizeof(int) * n));
    int j = 0;
    for (int i = 0; i < n; ++i)
    {
        if (T[i] < 0)
        {
            tab[j] = T[i];
            j++;
        }
    }
    return tab;
}