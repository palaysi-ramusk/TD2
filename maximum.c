#include "maximum.h"

int maximum(int T[], int n){
    int max = T[0];
    for (int i = 1; i < n; i++){
        if (T[i] > max){
            max = T[i];
        }
    }
    return max;
}