#include <stdio.h>
#include <matrice.h>

void affiche(struct Matrice m){
    for (int i = 0; i < m.nb_lignes; i++){
        for (int j = 0; j < m.nb_colonnes; j++){
            printf("%d ", m.valeurs[i][j]);
        }
        printf("\n");
    }
}