#include <malloc.h>

int *unsurdeux(int *tab, int n){
    int *tableau = malloc((sizeof(int) * n));
    if (tableau == NULL)
    {
        return NULL;
    }
    int j = 0;
    for (int i = 0; i < n; ++i)
    {
        if (i % 2 == 0)
        {
            tableau[j] = tab[i];
            j++;
        }
    }
    return tableau;
}